# irrs-treats

## SETTING UP IRRS UI USING DOCKER IMAGE TAR
### Note : Step 1 and 2 are one time task and to be performed only if the docker image is not present
1. Change to the directory containing the docker image tar and .env_gui_dev file.( path on Abzooba Kolkata server : /home/abzooba/IRRS/)
2. ```$ sudo docker load < irrs_ui.tar ```
3. ```$ sudo docker run --name irrs_ui --env-file .env_gui_dev -p 8084:3002 --network datalake  -d --restart always irrs:1.0.0```

## STOPPING THE UI SERVICE
1. ``` $ sudo docker stop irrs_ui```

## STARTING THE UI SERVICE
1. ``` $ sudo docker start irrs_ui```
