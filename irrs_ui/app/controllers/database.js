// Load module
var mysql = require('mysql');

var pool = mysql.createPool({
  connectionLimit: 20,
  host: process.env.host,
  port: process.env.port,
  user: process.env.user,
  password: process.env.password,
  database: process.env.database,
  debug: 'false'
});

try {
  pool.getConnection(function(err, connection) {
    if (err) {
      // winston.error(err);
      if (connection != 'undefined')
        connection.release();
      throw err;
    }
    connection.query('CREATE TABLE IF NOT EXISTS token(id bigint unsigned NOT NULL AUTO_INCREMENT, username VARCHAR(100), endpoints SET("GetSimilarTickets"), domain VARCHAR(20), expiry DATE, token VARCHAR(255),time timestamp NOT NULL default current_timestamp, primary key (id));', function(err, rows) {
    connection.query('CREATE TABLE IF NOT EXISTS user(id bigint unsigned NOT NULL AUTO_INCREMENT,logid varchar(50) NOT NULL,type varchar(50) ,username varchar(50) NOT NULL,api varchar(200) NOT NULL,dataset varchar(50),query TEXT CHARACTER SET utf8mb4 NOT NULL,result LONGTEXT CHARACTER SET utf8mb4 NOT NULL,time timestamp NOT NULL default current_timestamp,primary key (id));', function(err, rows) {
        connection.query('CREATE TABLE IF NOT EXISTS feedback(id bigint unsigned NOT NULL AUTO_INCREMENT,logid varchar(50) NOT NULL,username varchar(50) NOT NULL,query TEXT CHARACTER SET utf8mb4 NOT NULL,feedback TEXT CHARACTER SET utf8mb4 ,rating varchar(50),time timestamp NOT NULL default current_timestamp,primary key (id));', function(err, rows) {
          connection.query('CREATE TABLE IF NOT EXISTS config(id bigint unsigned NOT NULL AUTO_INCREMENT,dataset varchar(50) NOT NULL,api varchar(200) NOT NULL,columns LONGTEXT CHARACTER SET utf8mb4 NOT NULL,parameter LONGTEXT CHARACTER SET utf8mb4 ,primary key (id));',function(err,rows){
            connection.query('INSERT INTO config(dataset,api,columns,parameter) VALUES ("irrs_qa","http://172.16.4.51:32543/find_ticket",\'[{"title": "Incident Name","data": "name"},{"title": "URL","data": "url"},{"title": "Score","data": "score","sType" : "numeric"}]\',\'{"feedback" : "1","table" : "1","url":"1","summary" : "0","embedding" :"0" }\')',function(err,rows){
              connection.query('INSERT INTO config(dataset,api,columns,parameter) VALUES ("irrs_prod","http://172.16.4.51:32543/find_ticket",\'[{"title": "Incident Name","data": "name"},{"title": "URL","data": "url"},{"title": "Score","data": "score","sType" : "numeric"}]\',\'{"feedback" : "0","table" : "1","url":"0","summary" : "0","embedding" :"0" }\');',function(err,rows){
                connection.query('INSERT INTO token(username, endpoints, token) VALUES ("admin","GetSimilarTickets","test_token");',function(err,rows){
                  connection.release();
                });
              });
            });

          });
        });
      });
    });
    connection.on('error', function(err) {
      throw err;
    });
  });
} catch (err) {
  console.log("error in creating the table is ", err)
}


exports.executeQuery = function(query, callback) {
  try {
    pool.getConnection(function(err, connection) {

      if (err) {
        // winston.error(err);
        if (connection != 'undefined')
          connection.release();
        throw err;
      }
      connection.query(query, function(err, rows) {
        connection.release();
        if (!err) {
          callback(null, {
            rows: rows
          });
        }
      });
      connection.on('error', function(err) {
        throw err;

      });
    });
  } catch (err) {
    callback(err, null);
  }
};
