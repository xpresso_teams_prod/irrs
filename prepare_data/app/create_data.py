import os
import pandas as pd
from utils import text_prepare
import re
import configparser

config = configparser.RawConfigParser()
configFilePath = './config/config.properties'
config.read(configFilePath)
print(configFilePath)
starspace_training_data = []
path = config.get('PATH', 'mount_path')
print(path)
dataxlsx = config.get('DATAPATH', 'dataxlsx')
datadrive = config.get('DATAPATH', 'datadrive')
datapath = os.path.join(path, dataxlsx)
datadrive = os.path.join(path, datadrive)

import logging
import logging.config

logging.config.fileConfig(disable_existing_loggers=False, fname='./config/logging.conf')
logger = logging.getLogger(__name__)


def get_dataframe(data):
    columns = []
    df = pd.DataFrame(columns)
    # = os.fspath(data)
    flag = 0
    filenames = os.listdir(data)
    sorted_filenames = sorted(filenames)
    for filename in sorted_filenames:
        if filename.endswith(".xlsx"):
            logger.info("filename =  {}".format(filename))
            path = os.path.join(data, filename)
            reader = pd.ExcelFile(path)
            if flag == 0:
                if (len(reader.sheet_names) == 1):
                    df = reader.parse(reader.sheet_names[0])
                else:
                    df = reader.parse(reader.sheet_names[1])
                flag = 1
            else:
                if (len(reader.sheet_names) == 1):
                    new_df = reader.parse(reader.sheet_names[0])
                else:
                    new_df = reader.parse(reader.sheet_names[1])
                new_df.columns = df.columns
                df = df.append(new_df)
    return df


df = get_dataframe(datapath)

id_list = df['INCIDENT_ID'].tolist()
title_list = df['TITLE'].tolist()
desc_list = df['DESCRIPTION'].tolist()
ticket_details = []

ticket_details_map = {}
for i in range(0, len(title_list)):
    title = title_list[i]
    desc = desc_list[i]
    tkt_id = id_list[i]
    ticket_details_map[tkt_id] = {'title': title, 'description': desc}

filename = datadrive + '/incident_details.csv'
dirname = os.path.dirname(filename)

if not os.path.exists(dirname):
    os.makedirs(dirname)
import csv

fields = ['id', 'details']
with open(filename, 'w+') as f:
    writer = csv.DictWriter(f, fieldnames=fields)
    for k, v in ticket_details_map.items():
        writer.writerow({'id': k, 'details': v})
    f.close()

logger.info("created file =  {}".format(filename))

fields = ['id', 'details']
import ast

# generate title related files
title_list = []
tkt_details = []
import csv
import ast

with open(datadrive + '/incident_details.csv', 'r') as f1:
    reader = csv.DictReader(f1, fieldnames=fields)
    for row in reader:
        details = ast.literal_eval(row['details'])
        text = text_prepare(details['title'])
        sentences = text.split('\n')
        for sent in sentences:

            words = sent.split()
            if len(words) > 2:
                title_list.append(sent)
    f1.close()

from sklearn.feature_extraction.text import CountVectorizer

v = CountVectorizer(min_df=0.00001, max_df=1.0)

v.fit(desc_list)

vocab_set = set(v.get_feature_names())
complete_words = set()

for line in title_list:
    words = line.split()
    for word in words:
        complete_words.add(word)

stopwords = complete_words - vocab_set


def prepare_text(text):
    line = ' '.join([word for word in text.split() if word not in stopwords])
    return line


fields2 = ['sent', 'id']
from nltk import sent_tokenize
import csv

filename = datadrive + '/title_to_id_mapping_without_preprocessing.csv'
with open(datadrive + '/incident_details.csv', 'r') as f1:
    reader = csv.DictReader(f1, fieldnames=fields)
    with open(filename, 'w+') as f2:
        writer = csv.DictWriter(f2, fieldnames=fields2)
        for row in reader:
            details = ast.literal_eval(row['details'])
            text = details['title']
            sentences = text.split('\n')
            for sents in sentences:
                lines = sent_tokenize(sents)
                for line in lines:
                    if len(line.split()) > 1:
                        writer.writerow({'sent': line.encode("utf-8"), 'id': row['id']})

        f2.close()
    f1.close()

logger.info("created file =  {}".format(filename))

title_training_list = []
filename = datadrive + '/title_to_id_mapping_preprocessed.csv'
with open(datadrive + '/incident_details.csv', 'r') as f1:
    reader = csv.DictReader(f1, fieldnames=fields)
    with open(filename, 'w+') as f2:
        writer = csv.DictWriter(f2, fieldnames=fields2)
        for row in reader:
            details = ast.literal_eval(row['details'])
            text = details['title']
            sentences = text.split('\n')
            complete_sentence = ''
            for sents in sentences:
                lines = sent_tokenize(sents)

                for line in lines:
                    new_sent = text_prepare(line)
                    new_sent = prepare_text(new_sent)
                    new_sent = re.sub('_', ' ', new_sent)
                    words = new_sent.split()
                    if len(words) > 1:
                        writer.writerow({'sent': new_sent, 'id': row['id']})
                        title_training_list.append(new_sent)
                        complete_sentence = complete_sentence + '\t' + new_sent
            complete_sentence = complete_sentence.strip()
            starspace_training_data.append(complete_sentence)
        f2.close()
    f1.close()

logger.info("created file =  {}".format(filename))

title_unique_set = set()
filename = datadrive + '/title_data_splitted.txt'
with open(filename, 'a') as f:
    for sample in title_training_list:
        f.write(sample + '\n')
        title_unique_set.add(sample)
    f.close()

logger.info("created file =  {}".format(filename))

filename = datadrive + '/title_data_training.txt'
with open(filename, 'a') as f:
    for sample in title_unique_set:
        f.write(sample + '\n')
    f.close()

logger.info("created file =  {}".format(filename))

# generate desc related files


desc_list = []
tkt_details = []
import csv
import ast

with open(datadrive + '/incident_details.csv', 'r') as f1:
    reader = csv.DictReader(f1, fieldnames=fields)
    for row in reader:
        details = ast.literal_eval(row['details'])
        text = text_prepare(details['description'])
        sentences = text.split('\n')
        for sent in sentences:

            words = sent.split()
            if len(words) > 2:
                desc_list.append(sent)
    f1.close()

v = CountVectorizer(min_df=0.00001, max_df=1.0)

v.fit(desc_list)

vocab_set = set(v.get_feature_names())
complete_words = set()

for line in desc_list:
    words = line.split()
    for word in words:
        complete_words.add(word)

stopwords = complete_words - vocab_set

filename = datadrive + '/desc_to_id_mapping_without_preprocessing.csv'
with open(datadrive + '/incident_details.csv', 'r') as f1:
    reader = csv.DictReader(f1, fieldnames=fields)
    with open(filename, 'a') as f2:
        writer = csv.DictWriter(f2, fieldnames=fields2)
        for row in reader:
            details = ast.literal_eval(row['details'])
            text = details['description']
            sentences = text.split('\n')
            for sents in sentences:
                lines = sent_tokenize(sents)
                for line in lines:
                    if len(line.split()) > 1:
                        writer.writerow({'sent': line.encode("utf-8"), 'id': row['id']})

        f2.close()
    f1.close()

logger.info("created file =  {}".format(filename))

desc_training_list = []
filename = datadrive + '/desc_to_id_mapping_preprocessed.csv'
with open(datadrive + '/incident_details.csv', 'r') as f1:
    reader = csv.DictReader(f1, fieldnames=fields)
    with open(filename, 'a') as f2:
        writer = csv.DictWriter(f2, fieldnames=fields2)
        for row in reader:
            details = ast.literal_eval(row['details'])
            text = details['description']
            sentences = text.split('\n')
            complete_sentence = ''
            for sents in sentences:
                lines = sent_tokenize(sents)

                for line in lines:
                    new_sent = text_prepare(line)
                    new_sent = prepare_text(new_sent)
                    new_sent = re.sub('_', ' ', new_sent)
                    words = new_sent.split()
                    if len(words) > 1:
                        writer.writerow({'sent': new_sent, 'id': row['id']})
                        desc_training_list.append(new_sent)
                        complete_sentence = complete_sentence + '\t' + new_sent
            complete_sentence = complete_sentence.strip()
            starspace_training_data.append(complete_sentence)
        f2.close()
    f1.close()

logger.info("created file =  {}".format(filename))

desc_unique_set = set()
filename = datadrive + '/desc_data_splitted.txt'
with open(filename, 'a') as f:
    for sample in desc_training_list:
        f.write(sample + '\n')
        desc_unique_set.add(sample)
    f.close()

logger.info("created file =  {}".format(filename))

filename = datadrive + '/desc_data_training.txt'
with open(filename, 'a') as f:
    for sample in desc_unique_set:
        f.write(sample + '\n')
    f.close()
logger.info("created file =  {}".format(filename))

combined_set = title_unique_set.union(desc_unique_set)

filename = datadrive + '/combined_data_training.txt'
with open(filename, 'a') as f:
    for sample in combined_set:
        f.write(sample + '\n')
    f.close()

logger.info("created file =  {}".format(filename))

filename = datadrive + '/starspace_training.txt'
with open(filename, 'a') as f:
    for sample in starspace_training_data:
        f.write(sample + '\n')
    f.close()
logger.info("created file =  {}".format(filename))
