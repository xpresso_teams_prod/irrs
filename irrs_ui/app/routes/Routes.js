'use strict';

var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('../swagger.json');
var swaggerDocument_token = require('../swagger_token.json');
var usertable = require('../controllers/Usertable');
var feedback = require('../controllers/feedback');
var static_page = require('../controllers/static');

module.exports = function(app) {
  var auth = require('../controllers/utility');
  var apirouter = require('../controllers/Controller');
  var options = {
    customCss: 'swagger-ui .topbar .topbar-wrapper, .swagger-ui .topbar a { display: none }'
  }

  app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
  app.use('/token', swaggerUi.serve, swaggerUi.setup(swaggerDocument_token, options));

  //Front end pages
  app.route('/home')
    .get(static_page.gethomepage);

  app.route('/usertable')
    .get(static_page.getuserpage);

  //API Endpoints
  app.route('/gettoken')
    .post(auth.gettoken);

  app.route('/getUserTable')
    .post(usertable.getUserTable);

  app.route('/GetTickets')
    .post(apirouter.getticket);

  app.route('/download')
    .get(usertable.download);

  app.route('/addfeedback')
    .post(feedback.addfeedback);

  app.route('/feedbackdata')
    .post(feedback.feedbackdata);

  // app.route('/querydata')
  // .post(apirouter.querydata);

  app.route('/getembedding')
    .post(apirouter.getembedding);

  app.use(auth.utility);

  app.route('/GetSimilarTickets')
    .post(apirouter.GetSimilarTickets);

};
