var request = require('request');
var database = require('./database');
var fs = require('fs') ;

function download(req,res){
  var logid = req.query.logid;
  var query = "select log_id  as logid, type as type, username  as username,api as api,time as time,query as query, results as results from user where log_id='" + logid + "';" ;
  database.executeQuery(query,function(err,data){
    if(err){
      console.log(err);
      return;
    }
    if(data.rows.length == 0){
      res.status(204).send("No data available");
    }
    var results = JSON.parse(data.rows[0].results);
    var aoj = [];
    var qoj = {};
    qoj["logid"] = data.rows[0].logid;
    qoj["type"] = data.rows[0].type;
    qoj["username"] = data.rows[0].username;
    qoj["api"] = data.rows[0].api;
    qoj["time"] = data.rows[0].time;
    qoj["query"] = data.rows[0].query;

    for(var i=0;i<results.length;i++){
      aoj[i] = {};
      aoj[i]["name"] = results[i]["name"];
      aoj[i]["url"] = results[i]["url"];
      aoj[i]["score"] = results[i]["score"]
    }
    res.send([aoj,[qoj]]);
  });
}

function querydata(req, res) {
  try {
    var start = req.body.start;
    var end = req.body.end;
    var query = "select type,username,api,query from user where date(time) >='" + start + "' and date(time) <='" + end + "' order by time desc;";
    database.executeQuery(query, function(err, data) {
      if (err) {
        console.log(err);
        res.status(503).send({
          "error": "unable to retrieve feedback data"
        });
        return;
      }
      res.status(200).send(data.rows)
    });
  } catch (e) {
    console.log("error ", e)
    res.status(400).send()
  }
}

/*this function sends data of user information */
function getUserTable(request, response) {
    // winston.info("In User Table") ;
    var start = request.body.start;
    var end = request.body.end;

    var query = "select log_id  as logid, type as type, username  as username,api as api,query as query, results as results,time as time from user where date(time) >='" + start + "' and date(time) <='" + end + "' order by time desc;" ;

    database.executeQuery(query,function(err,data){
        // winston.info("query: " + query) ;
        if(err){
            // winston.error(err);
            return;
        }
        if(data.length===0){
          return response.send("Not data available");
        }
        var i,data1=[];
        for(i=0;i<data.rows.length;i++){

            data1.push([
                data.rows[i].logid,
                data.rows[i].type,
                data.rows[i].username,
                data.rows[i].api,
                '<a href="#" onclick="saveFile(\'' + data.rows[i].logid + '\')">Click Here</a>',
                data.rows[i].time
            ]);
        }
        response.send(data1);
    });
};

module.exports = {
  getUserTable: getUserTable,
  download : download,
  querydata : querydata
}
