# -*- coding: utf-8 -*-

from __future__ import division, print_function, unicode_literals

import json

import numpy as np
import tensorflow as tf
import utils
from autoencoder import TextAutoencoder

"""
Run the encoder part of the autoencoder in a corpus to generate
the memory cell representation for them.
"""


def load_model(directory, embeddings, session):
    model_path = os.path.join(directory, 'model')
    metadata_path = os.path.join(directory, 'metadata.json')
    with open(metadata_path, 'r') as f:
        metadata = json.load(f)
    dummy_embeddings = embeddings
    ae = TextAutoencoder(metadata['num_units'], dummy_embeddings,
                         metadata['go'], train=False,
                         bidirectional=metadata['bidirectional'])
    vars_to_load = ae.get_trainable_variables()
    vars_to_load.remove(ae.embeddings)
    saver = tf.train.Saver(vars_to_load)
    saver.restore(session, model_path)
    return ae


import configparser
import os


def create_embeddings(input_datapath, output_datapath):
    # generate desc embeddings
    datapath = input_datapath
    with open(datapath, 'r') as f:
        complete_sentences = f.readlines()

    count = 0
    if len(complete_sentences) <= 800000:
        number_of_chunks = 1
    else:
        number_of_chunks = int(len(complete_sentences) / 800000)
    print("number of title chunks = " + str(number_of_chunks))

    sentences = []
    if len(complete_sentences) < 800000:
        sentences.append(complete_sentences)
    else:
        p = 0
        q = 800000
        for i in range(0, number_of_chunks):
            sentences.append(complete_sentences[p:q])
            p = q
            q = q + 800000

    output_path = output_datapath

    if not os.path.exists(output_path):
        print('directory does not exist')
        os.makedirs(output_path)
    print(output_path)
    if os.path.exists(desc_output_path):
        print('directory exist now')
    # print(sentences)
    for i in range(0, len(sentences)):
        transformed_sentences, sizes = utils.load_text_data_list(sentences[i], wd)
        embedding = output_path + '/embedding' + str(i) + '.npy'
        print(embedding)
        next_index = 0
        all_states = []
        while next_index < len(transformed_sentences):
            print(count)
            batch = transformed_sentences[next_index:next_index + num_sents]
            batch_sizes = sizes[next_index:next_index + num_sents]
            next_index += num_sents
            state = model.encode(sess, batch, batch_sizes)
            all_states.append(state)
            count += 1
        state = np.vstack(all_states)
        np.save(embedding, state)


config = configparser.RawConfigParser()
configFilePath = 'config/config.properties'
config.read(configFilePath)
print(configFilePath)
path = config.get("PATH", 'mount_path')
model_path = config.get('CODIFY', 'model_path')
title_data = os.path.join(path, config.get('CODIFY', 'title_data'))
desc_data = os.path.join(path, config.get('CODIFY', 'desc_data'))
vocab = os.path.join(path, config.get('CODIFY', 'vocab'))
title_output_path = os.path.join(path, config.get('CODIFY', 'title_path'))
desc_output_path = os.path.join(path, config.get('CODIFY', 'desc_path'))
wd = utils.WordDictionary(vocab)
print('vocab loaded')
num_sents = 500
print(model_path)
m = os.path.join('/data', model_path)
sess = tf.InteractiveSession()
model = TextAutoencoder.load(m, sess)
# generate title embedding
create_embeddings(title_data, title_output_path)
create_embeddings(desc_data, desc_output_path)
