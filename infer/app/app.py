from __future__ import division, print_function, unicode_literals
import json

from autoencoder import TextAutoencoder
from utils import text_prepare

import numpy as np
import tensorflow as tf
import faiss
from faiss import normalize_L2
import utils
from flask import Flask, request, jsonify
import configparser
import os

path = '/data'
config = configparser.RawConfigParser()
configFilePath = 'config/config.properties'
config.read(configFilePath)
print(configFilePath)

from xpresso.ai.core.logging.xpr_log import XprLogger

logger = XprLogger('app/xpr_log_stage.json')

import nltk

nltk.download('punkt')
nltk.download('stopwords')

server = config.get('ENDPOINT', 'server')
port = config.get('ENDPOINT', 'port1')
embedding_size = 2 * int(config.get('AutoEncoder', 'lstm_units'))
# get title embeddings , vocabulary and model from config
model_path = config.get('ENCODER', 'model_path')
vocabulary = config.get('ENCODER', 'vocabulary')
title_embedding_path = config.get('ENCODER', 'title_embedding')
desc_embedding_path = path + '/' + config.get('ENCODER', 'desc_embedding')

# process title section
title_id_list = []
fields = ['sent', 'id']
count = 0
result = {}
import csv

cmd = 'cp -r data/* /data'
os.system(cmd)

with open('/data/datadrive/title_to_id_mapping_preprocessed.csv', 'r') as f:
    reader = csv.DictReader(f, fieldnames=fields)
    for row in reader:
        title_id_list.append(row['id'])
    f.close()
logger.info('loaded title mapping')

complete_desc_id_list = []
fields = ['sent', 'id']
count = 0
result = {}
import csv

with open('/data/datadrive/desc_to_id_mapping_preprocessed.csv', 'r') as f:
    reader = csv.DictReader(f, fieldnames=fields)
    for row in reader:
        complete_desc_id_list.append(row['id'])
    f.close()

desc_index = faiss.IndexFlatIP(embedding_size)

#############load desc embeddings#############
filenames = os.listdir(desc_embedding_path)
sorted_filenames = sorted(filenames)

for i in range(0, len(sorted_filenames) - 1):
    filename = sorted_filenames[i]
    if filename.endswith(".npy"):
        embedding_path = desc_embedding_path + '/' + filename
        embedding = np.load(embedding_path)
        normalize_L2(embedding)
        desc_index.add(embedding)
        del embedding

logger.info('loaded desc mapping')

# map title id to embeddings


title_index = faiss.IndexFlatIP(embedding_size)
#############load title embedding ################
for filename in os.listdir(os.path.join(path, title_embedding_path)):
    if filename.endswith(".npy"):
        embedding_path = title_embedding_path + '/' + filename
        embedding = np.load(os.path.join(path, embedding_path))
        normalize_L2(embedding)
        title_index.add(embedding)
        del embedding

logger.info('loaded title to vec maping')

sess = tf.InteractiveSession()
model = TextAutoencoder.load(os.path.join(path, model_path), sess)
logger.info('loaded title model')
app = Flask(__name__)

import csv

fields = ['id', 'text']
id_list = []
count = 0

fields = ['id', 'details']
ticket_details = {}

with open('/data/datadrive/incident_details.csv', 'r') as f:
    reader = csv.DictReader(f, fieldnames=fields)
    for row in reader:
        ticket_details[row['id']] = row['details']
    f.close()

logger.info('incident details loaded')

from nltk import sent_tokenize


def prepare_text(s):
    lines = s.split('\n ')
    sents = []
    print(lines)
    for line in lines:
        new_sents = sent_tokenize(line)
        for sent in new_sents:
            sents.append(text_prepare(sent))
    return sents


def get_sentence_embedding(line, vocabulary, model):
    sent = []
    sent.append(line)
    wd = utils.WordDictionary(vocabulary)
    sentences, sizes = utils.load_text_data_list(sent, wd)
    num_sents = 1
    next_index = 0
    all_states = []
    while next_index < len(sentences):
        batch = sentences[next_index:next_index + num_sents]
        batch_sizes = sizes[next_index:next_index + num_sents]
        next_index += num_sents
        state = model.encode(sess, batch, batch_sizes)
        all_states.append(state)

    state = np.vstack(all_states)
    return state


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)


def result_by_count(ticket_list):
    tkt_count_map = {}
    for sample in ticket_list:
        tkt_id = sample[0]
        if tkt_id in tkt_count_map.keys():
            tkt_count_map[tkt_id] = tkt_count_map[tkt_id] + 1
        else:
            tkt_count_map[tkt_id] = 1
    tkt_score_map = {}
    for sample in ticket_list:
        tkt_id = sample[0]
        score = sample[1]
        if tkt_id in tkt_score_map.keys():
            current_score = tkt_score_map[tkt_id]
            if score < current_score:
                tkt_score_map[tkt_id] = score
        else:
            tkt_score_map[tkt_id] = score
    cnt_tkt_map = {}
    for k, v in tkt_count_map.items():
        if v in cnt_tkt_map:
            count_list = cnt_tkt_map[v]
            count_list.append((k, tkt_score_map[k]))
            cnt_tkt_map[v] = count_list
        else:
            cnt_tkt_map[v] = [(k, tkt_score_map[k])]
    tkt_count_list = sorted(cnt_tkt_map.keys(), reverse=True)
    final_list = []
    i = 0
    while i < len(tkt_count_list) and len(final_list) < 50:
        cur_count = tkt_count_list[i]

        cur_count_list = cnt_tkt_map[cur_count]

        cur_count_list.sort(key=lambda x: x[1], reverse=True)

        for item in cur_count_list:
            final_list.append(item)
            if len(final_list) >= 50:
                break
        i += 1
    return final_list


def line_valid(line, wd):
    words = line.split()
    count = 0
    for w in words:
        if w in wd:
            count += 1
    if count >= 2:
        return 1
    else:
        return 0


import re


def find_closest_desc(sent, vocabulary, model):
    result = []
    lines = prepare_text(sent)
    wd = utils.WordDictionary(vocabulary)
    count = 0
    for line in lines:
        line = re.sub('_', ' ', line)

        valid = line_valid(line, wd)
        if valid == 1:

            vec1 = get_sentence_embedding(line, vocabulary, model)[0]
            if count == 0:
                vec = np.reshape(vec1, (1, 512))
                normalize_L2(vec)
                count += 1
            else:
                vec1 = np.reshape(vec1, (1, 512))
                normalize_L2(vec1)
                vec = np.append(vec, vec1, axis=0)

    D, I = desc_index.search(vec, 100)
    result = []
    for i in range(0, len(D)):
        for j in range(0, len(D[0])):
            result.append((I[i][j], D[i][j]))

    final_result = result_by_count(result)
    return final_result


def prepare_result(data, flag):
    final_result = []
    for sample in data:
        tkt_idx = sample[0]
        score = sample[1]
        if flag == 0:
            tkt_id = complete_desc_id_list[tkt_idx]
        else:
            if tkt_idx >= len(title_id_list):
                continue
            tkt_id = title_id_list[tkt_idx]
        tkt_details = ticket_details[tkt_id]
        details = {}
        details['incident'] = tkt_id
        details['score'] = np.float32(score)
        details['Title & Desc'] = tkt_details
        final_result.append(details)
    return final_result


def find_closest_titles(sent, vocabulary, model):
    result = []
    lines = prepare_text(sent)
    print(lines)
    wd = utils.WordDictionary(vocabulary)
    count = 0
    print(vocabulary)
    for line in lines:
        line = re.sub('_', ' ', line)
        print(line)
        valid = line_valid(line, wd)
        if valid == 1:
            print(line)
            vec1 = get_sentence_embedding(line, vocabulary, model)[0]
            if count == 0:
                vec = np.reshape(vec1, (1, 512))
                normalize_L2(vec)
                count += 1
            else:
                vec1 = np.reshape(vec1, (1, 512))
                normalize_L2(vec1)
                vec = np.append(vec, vec1, axis=0)
    if count == 0:
        final_result = []
    else:
        D, I = title_index.search(vec, 100)
        print(D)
        print(I)
        result = []
        for i in range(0, len(D)):
            for j in range(0, len(D[0])):
                result.append((I[i][j], D[i][j]))

        final_result = result_by_count(result)
    return final_result


def get_merged_ticket_list(ticket_list):
    ticket_score_map = {}
    for t in ticket_list:
        tkt_id = t[0]
        score = t[1]
        if tkt_id in ticket_score_map.keys():
            if ticket_score_map[tkt_id] < score:
                ticket_score_map[tkt_id] = score
        else:
            ticket_score_map[tkt_id] = score
    sorted_ticket_map = sorted(ticket_score_map.items(), key=lambda kv: kv[1])
    return sorted_ticket_map[0:10]


@app.route('/find_ticket', methods=['Post'])
def find_tickets():
    content = request.get_json()
    dictionary = {}
    title = content['title']
    desc = content['description']

    title = re.sub(r'\\n', '\n', title)
    desc = re.sub(r'\\n', '\n', desc)
    print(title)
    if len(title) > 2 and len(desc) > 5:
        msg = 'ok'
        result1 = find_closest_titles(title, vocabulary, model)
        final_result1 = prepare_result(result1, 1)
        result2 = find_closest_desc(desc, vocabulary, model)
        final_result2 = prepare_result(result2, 0)
        final_result = []
        final_result = final_result1[0:5]
        final_result.extend(final_result2[0:5])
        dictionary = {'result': final_result, 'messages': msg}
    elif len(title) > 2:
        print("finaind title")
        msg = 'ok'
        result = find_closest_titles(title, vocabulary, model)
        print(result)
        final_result = prepare_result(result, 1)
        dictionary = {'result': final_result, 'messages': msg}

    elif len(desc) > 5:
        msg = 'ok'
        result = find_closest_desc(desc, vocabulary, model)
        final_result = prepare_result(result, 0)
        dictionary = {'result': final_result, 'messages': msg}
    else:
        msg = 'no valid title &  desc'
        dictionary = {'result': [], 'messages': msg}
        return json.dumps(dictionary, cls=MyEncoder)

    return json.dumps(dictionary, cls=MyEncoder)


if __name__ == '__main__':
    app.run(host=server, port=port, debug=True, threaded=False)
