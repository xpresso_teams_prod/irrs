'use strict';
// var configuration = require('../../config/index.js')(process.argv[2]);
var app = angular.module('app', ['slick']);
// var name = configuration.get('name');
// var image = configuration.get('image');

app.config(['$locationProvider', function($locationProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
}])

app.controller('SessionTranscript', function($scope, $http, $location) {
  var session_id = $location.search().session_id;
  console.log(session_id);
  $scope.message = "Transcript Visualiser";
  // $scope.name = name;
  // $scope.image = image;
  $http({
    method: 'POST',
    url: '/tab8/getSessionTranscript',
    data: {
      'session_id': session_id
    }
  }).then(function successCallback(response) {
    $scope.data = response.data;
    console.log($scope.data);
  }, function errorCallback(response) {
    console.log(response.data);
  });
});

app.controller('UserTranscript', function($scope, $http, $location) {
  var user_id = $location.search().user_id;
  $scope.message = "Transcript Visualiser";
  $http({
    method: 'POST',
    url: '/tab7/getUserTranscript',
    data: {
      'user_id': user_id
    }
  }).then(function successCallback(response) {
    $scope.data = response.data;
  }, function errorCallback(response) {
    console.log(response.data);
  });
});

app.controller('messageFunnel', function($scope, $http, $location) {
  var msgChat = $location.search().msgChat;
  $scope.message = "Message Funnel";
  $http({
    method: 'POST',
    url: '/tab6/getMessageFunnel',
    data: {
      'msgChat': msgChat
    }
  }).then(function successCallback(response) {
    $scope.data = response.data;
    console.log($scope.data)
  }, function errorCallback(response) {
    console.log(response.data);
  });
});
