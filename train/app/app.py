# -*- coding: utf-8 -*-

from __future__ import print_function, division

"""
Script for training the autoencoder.
"""

import tensorflow as tf
import numpy as np

import utils
import autoencoder

from gensim.models import KeyedVectors

import configparser
import os

print(os.getcwd())
print(os.path.dirname(os.path.abspath(__file__)))
config = configparser.RawConfigParser()
configFilePath = "./config/config.properties"
config.read(configFilePath)
print(configFilePath)
path = config.get("PATH", 'mount_path')
model_path = path + '/' + config.get('AutoEncoder', 'model_path')
print(model_path)
train = config.get('AutoEncoder', 'train')
valid = config.get('AutoEncoder', 'valid')
vocab = config.get('AutoEncoder', 'vocab')
epoch = int(config.get('AutoEncoder', 'epoch'))
embedding_size = int(config.get('AutoEncoder', 'embedding_size'))
lstm_units = int(config.get('AutoEncoder', 'lstm_units'))
batch_size = int(config.get('AutoEncoder', 'batch_size'))
learning_rate = float(config.get('AutoEncoder', 'learning_rate'))
dropout = float(config.get('AutoEncoder', 'dropout'))
interval = int(config.get('AutoEncoder', 'interval'))
import logging.config

logging.config.fileConfig(disable_existing_loggers=False, fname='./config/logging.conf')
logger = logging.getLogger(__name__)
logger.info("number of epochs =  {}".format(epoch))
if not os.path.exists(model_path):
    print('directory does not exist')
    os.makedirs(model_path)

if os.path.exists(model_path):
    print('directory exist now')


def show_parameter_count(variables):
    """
    Count and print how many parameters there are.
    """
    total_parameters = 0
    for variable in variables:
        name = variable.name

        # shape is an array of tf.Dimension
        shape = variable.get_shape()
        variable_parametes = 1
        for dim in shape:
            variable_parametes *= dim.value
        print('{}: {} ({} parameters)'.format(name,
                                              shape,
                                              variable_parametes))
        total_parameters += variable_parametes

    print('Total: {} parameters'.format(total_parameters))


def load_or_create_embeddings(path, wd, embedding_size):
    """
    If path is given, load an embeddings file. If not, create a random
    embedding matrix with shape (vocab_size, embedding_size)
    """
    if path is not None:
        model = KeyedVectors.load_word2vec_format(path)
        words = wd.d.keys()
        len(words)
        vec_words = []
        for word in model.vocab:
            vec_words.append(word)
        embedding = np.array([])
        count = 0
        for word in words:
            if word in vec_words:
                embedding = np.append(embedding, model[word])
                count += 1
            else:
                vec = np.random.uniform(-5, 5, (1, 300))
                embedding = np.append(embedding, vec)
        print(count)
        embedding = embedding.reshape(len(wd.d), 300)
        return embedding.astype(np.float32)

    embeddings = np.random.uniform(-0.1, 0.1, (len(wd.d), embedding_size))
    return embeddings.astype(np.float32)


'''
class_ = argparse.ArgumentDefaultsHelpFormatter
parser = argparse.ArgumentParser(description=__doc__,
                                 formatter_class=class_)
#parser.add_argument('save_dir', help='Directory to file to save trained '
#                                     'model')
parser.add_argument('-n', help='Embedding size (if embeddings not given)',
                    default=300, dest='embedding_size', type=int)
parser.add_argument('-u', help='Number of LSTM units (when using a '
                               'bidirectional model, this is doubled in '
                               'practice)', default=500,
                    dest='lstm_units', type=int)
parser.add_argument('-r', help='Initial learning rate', default=0.001,
                    dest='learning_rate', type=float)
parser.add_argument('-b', help='Batch size', default=32,
                    dest='batch_size', type=int)
#parser.add_argument('-e', help='Number of epochs', default=100,
#                    dest='num_epochs', type=int)
parser.add_argument('-d', help='Dropout keep probability', type=float,
                    dest='dropout_keep', default=1.0)
parser.add_argument('-i',
                    help='Number of batches between performance report',
                    dest='interval', type=int, default=1000)
parser.add_argument('--mono', help='Use a monodirectional LSTM '
                                   '(bidirectional is used by default)',
                    action='store_false', dest='bidirectional')
parser.add_argument('--te', help='Train embeddings. If not given, they are '
                                 'frozen. (always true if embeddings are '
                                 'not given)',
                    action='store_true', dest='train_embeddings')
parser.add_argument('--embeddings',
                    help='Numpy embeddings file. If not supplied, '
                         'random embeddings  are generated.')

parser.add_argument('vocab', help='Vocabulary file')
parser.add_argument('train', help='Training set')
parser.add_argument('valid', help='Validation set')
'''

logging.basicConfig(level=logging.INFO)

sess = tf.Session()
wd = utils.WordDictionary(os.path.join(path, vocab))
embeddings = load_or_create_embeddings(None, wd,
                                       embedding_size)

logging.info('Reading training data')
train_data = utils.load_binary_data(os.path.join(path, train))
logging.info('Reading validation data')
valid_data = utils.load_binary_data(os.path.join(path, valid))
logging.info('Creating model')

train_embeddings = True
model = autoencoder.TextAutoencoder(lstm_units,
                                    embeddings, wd.eos_index,
                                    train_embeddings=train_embeddings,
                                    bidirectional=True)

sess.run(tf.global_variables_initializer())
show_parameter_count(model.get_trainable_variables())
logging.info('Initialized the model and all variables. Starting training.')
model.train(sess, model_path, train_data, valid_data, batch_size,
            epoch, learning_rate,
            dropout, 5.0, report_interval=interval)
