/**
 * Created by rounak on 29/5/17.
 */
function lineChart(filter,url,type,container,title,subtitle,yAxisTitle){
    var startDate = new Date(filter.start);
    filter.type = type;
    var seriesData;
    $.ajax({
        url: url,
        type: "POST",
        data: filter,
        success: function(data) {
            Highcharts.chart(container, {

                title: {
                    text: title
                },
                subtitle: {
                    text: subtitle
                },
                yAxis: {
                    title: {
                        text: yAxisTitle
                    }
                },

                xAxis: {
                    type:'datetime',
                    tickInterval: 2 * 24 * 3600 * 1000, // one week
                    tickWidth: 0,
                    gridLineWidth: 1,
                    labels: {
                        align: 'left',
                        x: 0,
                        y: 15
                    }
                },

                legend: {
                    align: 'center',
                    floating: true,
                    verticalAlign: 'bottom',
                    borderWidth: 0,
                    floating:true,
                    y:20
                },
                tooltip:{
                    crosshairs: true,
                    shared: true
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        pointStart: startDate.getTime(),
                        pointInterval: 24*3600*1000,
                        point: {
                            events: {
                                click: function (e) {
                                    hs.htmlExpand(null, {
                                        pageOrigin: {
                                            x: e.pageX || e.clientX,
                                            y: e.pageY || e.clientY
                                        },
                                        headingText: this.series.name,
                                        maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' + this.y,
                                        width: 200
                                    });
                                }
                            }
                        }
                    }
                },
                series: data

            });
        },
    });
}
