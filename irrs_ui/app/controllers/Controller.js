var validate = require('./validate');
var request = require('request');
var cheerio = require('cheerio');
var encodeUrl = require('encodeurl')
var database = require('./database');
var uuid = require('uuid/v1');
var jwt = require('jsonwebtoken');
var url = require('url');
var conf = require('../config');
var winston = require('../Loggerutil');
var normalizesql = require('./normalizesql').normalizesql;
const _ = require('lodash');

//Response of sendresponse will send an array with first element being array of json and the second being the logid
function sendresponse(body, db_data, req, res, form_data) {
  switch (db_data['dataset']) {
    case 'irrs':
      console.log(body)
      res.setHeader('statusCode', res.statusCode);
      var results = body['result']
      var resp = [];
      var name;
      Object.keys(results).forEach(function(key) {
        console.log(key)
        name = key;
        name = name.replace('\0', '');
        name = name.replace(/\0/g, '');
        resp.push({
          "name": results[key]["incident"],
          "url": results[key]["Title & Desc"],
          "score": parseFloat(results[key]["score"])
        })
      });
      return res.send([resp, db_data["logid"], form_data]);
      // db_data['query'] = db_data['query'].replace(/\'/g, '');
      /*var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";

      winston.info("Query value is :  " + query);
      database.executeQuery(query, function(err, rows) {
        // res.send([resp, text, url, form_data]);
        return res.send([resp, db_data["logid"], form_data]);
      });*/
      break;


      case 'sprint':
        body = JSON.parse(body);
        res.setHeader('statusCode', res.statusCode);
        var results = body.results;
        var resp = [];
        var name;
        var avoid_keys = []
        avoid_keys.push("filters");
        avoid_keys.push("preferred filters");
        avoid_keys.push("summary");
        Object.keys(results).forEach(function(key) {
          if (!_.includes(avoid_keys, key)) {
            name = key;
            name = name.replace('\0', '');
            name = name.replace(/\0/g, '');
            resp.push({
              "name": name,
              "TITLE": ((results[key].hasOwnProperty('TITLE')) ? results[key]["TITLE"] : ""),
              "score": ((results[key].hasOwnProperty('score')) ? parseFloat(results[key]["score"]) : ""),
              "INCIDENT_ID": ((results[key].hasOwnProperty('INCIDENT_ID')) ? results[key]["INCIDENT_ID"] : ""),
              "Incident Summary": ((results[key].hasOwnProperty('Incident Summary')) ? JSON.stringify(results[key]["Incident Summary"]) : ""),
              "Enriched Resolution Summary": ((results[key].hasOwnProperty('Enriched Resolution Summary')) ? JSON.stringify(results[key]["Enriched Resolution Summary"]) : ""),
              "Concept scores": ((results[key].hasOwnProperty('concept scores')) ? JSON.stringify(results[key]["concept scores"]) : ""),
              "AFFECTED_SERVICE": ((results[key].hasOwnProperty('AFFECTED_SERVICE')) ? JSON.stringify(results[key]["AFFECTED_SERVICE"]) : ""),
              "CI_TYPE": ((results[key].hasOwnProperty('CI_TYPE')) ? JSON.stringify(results[key]["CI_TYPE"]) : ""),
              "ASSIGNMENT_GROUP": ((results[key].hasOwnProperty('ASSIGNMENT_GROUP')) ? JSON.stringify(results[key]["ASSIGNMENT_GROUP"]) : ""),
              "SUB_AREA": ((results[key].hasOwnProperty('SUB_AREA')) ? JSON.stringify(results[key]["SUB_AREA"]) : ""),
              "AREA": ((results[key].hasOwnProperty('AREA')) ? JSON.stringify(results[key]["AREA"]) : ""),
              "PRIORITY": ((results[key].hasOwnProperty('PRIORITY')) ? JSON.stringify(results[key]["PRIORITY"]) : ""),
              "STATUS": ((results[key].hasOwnProperty('STATUS')) ? JSON.stringify(results[key]["STATUS"]) : "")
            })
          }
        });
        var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
          "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";

        winston.info("Query value is :  " + query);
        database.executeQuery(query, function(err, rows) {
          return res.send([resp, db_data["logid"], form_data]);
        });
        break;


    case 'anthem':

      let res_trans = {}
      body = JSON.parse(body);
      let cpt = ""
      for (let x in body['CPTINFO']) {
        x = x.replace('\t', ' ')
        cpt += body['CPTINFO'][x] + ". "
      }

      let ded = JSON.stringify(body['4 Deductible Type Annual'])
      ded = ded.replace(/"/g, "")
      ded = ded.replace("{", "")
      ded = ded.replace("}", "")
      //ded = ded.replace(",", "\n")
      res_trans['CPTINFO'] = ded

      let outofpocket = JSON.stringify(body['5 OUT-OF-POCKET ANNUAL MAXIMUM'])
      outofpocket = outofpocket.replace(/"/g, "")
      outofpocket = outofpocket.replace("}", "")
      outofpocket = outofpocket.replace("{", "")
      //outofpocket = outofpocket.replace(",", "\n")

      let inpatient = JSON.stringify(body['12 INPATIENT HOSPITAL'])
      inpatient = inpatient.replace(/"/g, "")
      inpatient = inpatient.replace("{", "")
      inpatient = inpatient.replace("}", "")
      //inpatient = inpatient.replace(",", "\n")

      let outpatient = JSON.stringify(body['13 OUTPATIENT/AMBULATORY SURGERY AT A FACILITY'])
      outpatient = outpatient.replace(/"/g, "")
      outpatient = outpatient.replace("}", "")
      outpatient = outpatient.replace("{", "")
      //outpatient = outpatient.replace(",", "\n")


      res_trans['CPTINFO'] = cpt
      res_trans['DEDUCTIBLE_TYPE_ANNUAL'] = ded
      res_trans['OUT-OF-POCKET_ANNUAL_MAXIMUM'] = outofpocket
      res_trans['INPATIENT_HOSPITAL'] = inpatient
      res_trans['OUTPATIENT_AMBULATORY_SURGERY_AT_A_FACILITY'] = outpatient
      var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(res_trans)) + "');";
      database.executeQuery(query, function(err, data) {
        return res.status(200).send([
          [res_trans], db_data["logid"], form_data
        ]);
      });
      break;

    case 'netapp':
      body = JSON.parse(body);
      var results = body.results;
      var resp = [],
        avoid_keys = [];
      var name;
      avoid_keys.push("filters");
      avoid_keys.push("preferred filters");

      Object.keys(results).forEach(function(key) {
        if (!_.includes(avoid_keys, key)) {
          name = key;
          name = name.replace('\0', '');
          name = name.replace(/\0/g, '');

          let summary_value = "";
          if (results[key].hasOwnProperty('summary')) {
            // summary_value = '<p>';
            let summary = JSON.stringify(results[key]["summary"]);
            summary = summary.slice(1, -1);
            let summary_split = summary.split(",");
            summary_value = summary_split.join("\r\n");
            // summary_value = summary_split.join("<br>");
            // summary_value = summary_value + '</p>';
          }

          resp.push({
            "Short description": ((results[key].hasOwnProperty('Short description')) ? results[key]["Short description"] : ""),
            "url": ((results[key].hasOwnProperty('url')) ? results[key]["url"] : ""),
            "score": ((results[key].hasOwnProperty('score')) ? parseFloat(results[key]["score"]) : ""),
            "summary": summary_value,
            "Incident Number": ((results[key].hasOwnProperty('Incident Number')) ? results[key]["Incident Number"] : ""),
            "Assignment group": ((results[key].hasOwnProperty('Assignment group')) ? results[key]["Assignment group"] : ""),
            "State": ((results[key].hasOwnProperty('State')) ? results[key]["State"] : ""),
            "Priority": ((results[key].hasOwnProperty('Priority')) ? results[key]["Priority"] : ""),
            "Resolution Summary": ((results[key].hasOwnProperty('Resolution Summary')) ? results[key]["Resolution Summary"] : ""),
            "Incident Summary": ((results[key].hasOwnProperty('Incident Summary')) ? results[key]["Incident Summary"] : ""),
            "Incident Type": ((results[key].hasOwnProperty('Incident Type')) ? results[key]["Incident Type"] : ""),
            "Effort Level": ((results[key].hasOwnProperty('Effort Level')) ? results[key]["Effort Level"] : ""),
            "Enriched Resolution Summary": ((results[key].hasOwnProperty('Enriched Resolution Summary')) ? results[key]["Enriched Resolution Summary"] : ""),
            "concept scores": ((results[key].hasOwnProperty('concept scores')) ? JSON.stringify(results[key]["concept scores"]) : "")
          });
        }
      });

      var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";
      winston.info("query for inserting data in user table : \n" + query);
      database.executeQuery(query, function(err, rows) {
        if (err) {
          winston.error("error in querying database in sendresponse ", err);
          res.status(503).send({
            "error": "unable to query database"
          });
        }
        return res.status(200).send([resp, db_data["logid"], form_data]);
      });
      break;


    case 'rockwell':
      body = JSON.parse(body);
      res.setHeader('statusCode', res.statusCode);
      var results = body.results;
      var resp = [];
      var name;
      Object.keys(results).forEach(function(key) {
        name = key;
        name = name.replace('\0', '');
        name = name.replace(/\0/g, '');
        resp.push({
          "name": name,
          "Type": ((results[key].hasOwnProperty('Type')) ? results[key]["Type"] : ""),
          "score": ((results[key].hasOwnProperty('score')) ? parseFloat(results[key]["score"]) : ""),
          "summary": ((results[key].hasOwnProperty('summary')) ? JSON.stringify(results[key]["summary"]) : ""),
        })
      });
      // db_data['query'] = db_data['query'].replace(/\'/g, '');
      var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";

      winston.info("Query value is :  " + query);
      database.executeQuery(query, function(err, rows) {
        // res.send([resp, text, url, form_data]);
        return res.send([resp, db_data["logid"], form_data]);
      });
      break;

    case 'finra':
      body = JSON.parse(body);
      res.setHeader('statusCode', res.statusCode);
      var results = body.results;
      var resp = [];
      var name;
      var avoid_keys = []
      avoid_keys.push("filters");
      avoid_keys.push("preferred filters");
      avoid_keys.push("summary");
      Object.keys(results).forEach(function(key) {
        if (!_.includes(avoid_keys, key)) {
          name = key;
          name = name.replace('\0', '');
          name = name.replace(/\0/g, '');
          resp.push({
            "name": name,
            "Resolution Date": ((results[key].hasOwnProperty('Resolution Date')) ? results[key]["Resolution Date"] : ""),
            "score": ((results[key].hasOwnProperty('score')) ? parseFloat(results[key]["score"]) : ""),
            "summary": ((results[key].hasOwnProperty('summary')) ? JSON.stringify(results[key]["summary"]) : ""),
            "Monetary Fine": ((results[key].hasOwnProperty('Monetary Fine')) ? JSON.stringify(results[key]["Monetary Fine"]) : ""),
            "Enriched Sanctions Summary": ((results[key].hasOwnProperty('Enriched Sanctions Summary')) ? JSON.stringify(results[key]["Enriched Sanctions Summary"]) : ""),
            "Enriched Allegations Summary": ((results[key].hasOwnProperty('Enriched Allegations Summary')) ? JSON.stringify(results[key]["Enriched Allegations Summary"]) : ""),
            "Docket Case Number": ((results[key].hasOwnProperty('Docket\/Case Number')) ? JSON.stringify(results[key]["Docket\/Case Number"]) : ""),
            "Reporting Source": ((results[key].hasOwnProperty('Reporting Source')) ? JSON.stringify(results[key]["Reporting Source"]) : ""),
            "Resolution": ((results[key].hasOwnProperty('Resolution')) ? JSON.stringify(results[key]["Resolution"]) : ""),
            "Current Status": ((results[key].hasOwnProperty('Current Status')) ? JSON.stringify(results[key]["Current Status"]) : ""),
            "Date Initiated": ((results[key].hasOwnProperty('Date Initiated')) ? JSON.stringify(results[key]["Date Initiated"]) : ""),
            "Crd": ((results[key].hasOwnProperty('Crd #')) ? JSON.stringify(results[key]["Crd #"]) : ""),
            "Broker Name": ((results[key].hasOwnProperty('Broker Name')) ? JSON.stringify(results[key]["Broker Name"]) : ""),
            "concept scores": ((results[key].hasOwnProperty('concept scores')) ? JSON.stringify(results[key]["concept scores"]) : "")
          })
        }
      });
      // db_data['query'] = db_data['query'].replace(/\'/g, '');
      var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";

      winston.info("Query value is :  " + query);
      database.executeQuery(query, function(err, rows) {
        // res.send([resp, text, url, form_data]);
        return res.send([resp, db_data["logid"], form_data]);
      });
      break;

    case 'salesforce':
      body = JSON.parse(body);
      res.setHeader('statusCode', res.statusCode);
      var results = body.results;
      var resp = [];
      var name;
      var avoid_keys = []
      avoid_keys.push("filters");
      avoid_keys.push("preferred filters");
      avoid_keys.push("summary");
      Object.keys(results).forEach(function(key) {
        if (!_.includes(avoid_keys, key)) {
          name = key;
          name = name.replace('\0', '');
          name = name.replace(/\0/g, '');
          resp.push({
            "name": name,
            "ID": ((results[key].hasOwnProperty('ID')) ? results[key]["ID"] : ""),
            "score": ((results[key].hasOwnProperty('score')) ? parseFloat(results[key]["score"]) : ""),
            "CaseNumber": ((results[key].hasOwnProperty('CaseNumber')) ? JSON.stringify(results[key]["CaseNumber"]) : ""),
            "Account Id": ((results[key].hasOwnProperty('Account Id')) ? JSON.stringify(results[key]["Account Id"]) : ""),
            "Type": ((results[key].hasOwnProperty('Type')) ? JSON.stringify(results[key]["Type"]) : ""),
            "Status": ((results[key].hasOwnProperty('Status')) ? JSON.stringify(results[key]["Status"]) : ""),
            "Reason": ((results[key].hasOwnProperty('Reason')) ? JSON.stringify(results[key]["Reason"]) : ""),
            "Origin": ((results[key].hasOwnProperty('Origin')) ? JSON.stringify(results[key]["Origin"]) : ""),
            "Subject": ((results[key].hasOwnProperty('Subject')) ? JSON.stringify(results[key]["Subject"]) : ""),
            "Priority": ((results[key].hasOwnProperty('Priority')) ? JSON.stringify(results[key]["Priority"]) : ""),
            "Owner Id": ((results[key].hasOwnProperty('Owner Id')) ? JSON.stringify(results[key]["Owner Id"]) : ""),
            "Created Date": ((results[key].hasOwnProperty('Created Date')) ? JSON.stringify(results[key]["Created Date"]) : ""),
            "Created By Id": ((results[key].hasOwnProperty('Created By Id')) ? JSON.stringify(results[key]["Created By Id"]) : ""),
            "Last Modified Date": ((results[key].hasOwnProperty('Last Modified Date')) ? JSON.stringify(results[key]["Last Modified Date"]) : ""),
            "Last Modified By Id": ((results[key].hasOwnProperty('Last Modified By Id')) ? JSON.stringify(results[key]["Last Modified By Id"]) : ""),
            "Product": ((results[key].hasOwnProperty('Product')) ? JSON.stringify(results[key]["Product"]) : "")
          })
        }
      });
      // db_data['query'] = db_data['query'].replace(/\'/g, '');
      var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";

      winston.info("Query value is :  " + query);
      database.executeQuery(query, function(err, rows) {
        // res.send([resp, text, url, form_data]);
        return res.send([resp, db_data["logid"], form_data]);
      });
      break;

    case 'salesforcearticles':
      body = JSON.parse(body);
      res.setHeader('statusCode', res.statusCode);
      var results = String(body);
	results = results.replace(/\.,/g,".<br>");
	var resp = []
      resp.push({
        "answer": results,
        "score":0,
        "score" : 0
      })
      // db_data['query'] = db_data['query'].replace(/\'/g, '');
      var query = "INSERT INTO user (logid, type,username,api,dataset,query,result) VALUES ('" + db_data["logid"] + "', '" + db_data["type"] +
        "','" + db_data["username"] + "','" + db_data["api"] + "','" + db_data['dataset'] + "','" + normalizesql(db_data["query"]) + "','" + normalizesql(JSON.stringify(resp)) + "');";

      winston.info("Query value is :  " + query);
      database.executeQuery(query, function(err, rows) {
        // res.send([resp, text, url, form_data]);
        return res.send([resp, db_data["logid"], form_data]);
      });
      break;
  }
}

function calldsrr(base_url, form_data, db_data, req, res) {

  winston.info("Radar endpoint : " + base_url);
  winston.info("DB Data : " + db_data['dataset']);
  switch (db_data['dataset']) {
    case 'irrs':
      winston.info(base_url)
      winston.info(form_data)
      winston.info(db_data)
      base_url="http://172.16.4.51:32543/find_ticket"
      request(base_url, {
          json: {"title":form_data['title'],"description":""},
          method: 'POST',
        },
        function(error, response, body) {
        console.log(response)
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          console.log(body)
          sendresponse(body, db_data, req, res, form_data);
        });
      break;

    case 'anthem':
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;

    case 'netapp':
      // if (!("summary_filter" in req.body) || _.isNil(req.body.summary_filter)) {
      //   res.status(404).send({
      //     "error": "Required parameter summary not found in the request"
      //   });
      // }
      if (req.body.summary_filter == "Resolution Summary") {
        form_data.status = form_data.status + " State Closed";
      }
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;
    case 'rockwell':
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;

    case 'finra':
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;
    case 'salesforce':
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;

    case 'salesforcearticles':
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;

    case 'sprint':
      request(base_url, {
          form: form_data,
          method: 'POST'
        },
        function(error, response, body) {
          if (response.statusCode !== 200 || error) {
            res.status(500).send();
            return;
          }
          sendresponse(body, db_data, req, res, form_data);
        });
      break;

  }
}

function parsedata(req, res, tiptop_url) {
  var body = req.body;
  let form_data = {};
  var url_parsed = url.parse(tiptop_url, true);
  var base_url = url_parsed.href;
  base_url = (base_url.startsWith('http') ? base_url : 'http://' + base_url)
  base_url = encodeUrl(base_url);
  if (!_.isNil(url_parsed.query.coll)) {
    form_data.coll = url_parsed.query.coll;
  }
  if (!_.isNil(url_parsed.query.config)) {
    form_data.config = url_parsed.query.config;
  }
  var text_retrieve_url = conf.text_retrieve_url;


  var db_data = {}; // contains logid,type,username,apiendpoint,query,result and timestamp
  db_data["type"] = "user";
  db_data["api"] = tiptop_url;
  // db_data["dataset"] = form_data.config; // Dataset values can be  : IRRSQ-netapp-ust , IRRS, anthem
  db_data["dataset"] = req.query.config.split('_')[0];
  
  //jwt.verify(req.body.token, conf.secret, function(err, decoded) {
  //  db_data["logid"] = uuid();
  //  db_data["username"] = decoded.id;
  //});
  winston.info(body)
  if (body['type'] == "text") {
    db_data['query'] = body['text'];
    form_data['title'] = body['text']
    form_data['description'] = body['text'] + " " + body['text'] + " " + body['text']  + " " + body['text']
    calldsrr(base_url, form_data, db_data, req, res);
  } else if (body['type'] == 'url') {
    var incident_url = body['text'];
    var filename = incident_url.match(/[^\/]+$/)[0];
    filename = filename + ".txt";
    request(text_retrieve_url + filename, function(error, response, body) {
      if (response.statusCode == 200) {
        winston.info("Data for url : " + incident_url + "  already present in http://radar.feeltiptop.com/demos/abzooba/IRRS-data/data/");
        db_data['query'] = response.body;
        form_data.status = response.body;
        calldsrr(base_url, form_data, db_data, req, res);
      } else {
        winston.info("Data for url : " + incident_url + "  not present in http://radar.feeltiptop.com/demos/abzooba/IRRS-data/data/");
        request(incident_url, function(error, response, html) {
          if (!error && response.statusCode == 200) {
            var str = '',
              $ = cheerio.load(html);

            let postTitle = $('div[class="post-title"]');
            if (!postTitle || !postTitle.length > 0) {
              winston.error('Unable to extract heading from report');
            } else {
              postTitle.each(function(i, element) {
                str = str + $(this).find("h1").text().trim() + ' ';
              });
            }

            let postBody = $('div[class="post-body"]');
            if (!postBody || !postBody.length > 0) {
              winston.error('Unable to extract body from report');
            } else {
              postBody.each(function(i, element) {
                str = str + $(this).text();
              });
            }
            db_data['query'] = str;
            form_data.status = str;
            calldsrr(base_url, form_data, db_data, req, res);
          }
        });
      }
    });
  }
}

function GetSimilarTickets(req, res, next) {
  var config_name = req.query.config;
  winston.info(config_name)
  winston.info(req.body)
  if (config_name.includes('qa')) { //for the qa version, retrieves the url from the url parameter
    if (!("url" in req.body) || _.isNil(req.body.url)) {
      res.statusCode(400).send({
        "error": "Required parameter url not present"
      });
      return;
    }
    var tiptop_url = req.body.url.trim();
    parsedata(req, res, tiptop_url)
  } else if (config_name.includes('prod')) { //in case of prod
    if (_.isNil(config_name)) {
      res.statusCode(400).send();
      return;
    }

    var query = "SELECT api from config where dataset = '" + config_name + "'";
    winston.info("Query for retrieving radar endpoint from config table : \n" + query);
    database.executeQuery(query, function(err, data) {
      if (err) {
        let errorResponse = "Unable to fetch results for " + dataset + " from db";
        res.status(400).send({
          "error": errorResponse
        });
      }
      if (data.rows.length == 0 || _.isNil(data.rows[0].api)) {
        res.status(400).send({
          "error": "unable to retrieve API from database"
        });
      }
      tiptop_url = data.rows[0].api;
      parsedata(req, res, tiptop_url)
    });

  }
};


function getembedding(req, res) {
  if (!("url" in req.body) || _.isNil(req.body.url) || !("type" in req.body) || _.isNil(req.body.type) || !("status" in req.body) || _.isNil(req.body.status)) {
    res.statusCode(400).send({
      "error": "Required parameter url not present"
    });
  }
  var callurl = req.body.url + '?' + 'config=IRRSQ-netapp-ust&type=' + req.body.type + '&status=' + req.body.status
  callurl = encodeUrl(callurl);
  request(callurl,
    method = 'GET',
    function(error, response, body) {
      if (response.statusCode !== 200 || error) {
        res.status(500).send();
        return;
      }
      res.send(response);
    });
}

module.exports = {
  GetSimilarTickets: GetSimilarTickets,
  getticket: GetSimilarTickets,
  getembedding: getembedding
}
